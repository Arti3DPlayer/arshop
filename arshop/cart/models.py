# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

class CartManager(models.Manager):
    def get_for_session(self, session):
        """
        Find cart associated with the session or create new cart.
        """

        pk = session.get('cart_pk')

        try:
            cart = self.get(pk=pk, checkout=False)
        except self.model.DoesNotExist:
            cart = None

        if not cart:
            cart = self.create()
            session['cart_pk'] = cart.pk

        return cart

def ct_lookup(obj):
    """
    Return ORM lookup arguments for refering to the obj when
    it is linked via GenericForeignKey.
    """

    return {'content_type': ContentType.objects.get_for_model(obj),
            'object_id': obj.pk,
            }

class Cart(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    checkout = models.BooleanField(blank=True, default=False)
    objects = CartManager()

    def add_item(self, product, quantity=1):
        try:
            item = self.items.get(**ct_lookup(product))
        except CartItem.DoesNotExist:
            item = CartItem(product=product, quantity=0, cart=self)
        item.quantity += quantity
        item.save()

    def delete_item(self, product):
        try:
            item = self.items.get(**ct_lookup(product))
            item.delete()
        except CartItem.DoesNotExist:
            pass


class CartItem(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    product = generic.GenericForeignKey()
    cart = models.ForeignKey(Cart, related_name='items')
    quantity = models.IntegerField(blank=True, default=1)
