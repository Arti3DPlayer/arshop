# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.db import models
from django.utils import simplejson
from django.views.generic.base import View
from django.core.serializers.json import DjangoJSONEncoder
from .models import Cart


class BaseCartView(View):
    availible_models = ['product', ]

    def cart_data(self, request, cart):
        cart_items = cart.items.all()
        request.session.modified = True
        data = {}
        data['quantity'] = sum(x.quantity for x in cart_items)
        data['total_price'] = sum(float(x.product.price*x.quantity) for x in cart_items)
        return data


class CartGetView(BaseCartView):
    def post(self, request, *args, **kwargs):
        cart = Cart.objects.get_for_session(request.session)
        data = self.cart_data(request, cart)
        return HttpResponse(simplejson.dumps(data, cls=DjangoJSONEncoder))


class CartAddView(BaseCartView):
    def post(self, request, *args, **kwargs):
        app_label = request.POST.get('app_label')
        model_name = request.POST.get('model')
        object_id = request.POST.get('id')
        model = models.get_model(app_label, model_name)

        if model and model_name in self.availible_models:
            product = get_object_or_404(model, id=object_id)
            cart = Cart.objects.get_for_session(request.session)
            cart.add_item(product)
            data = self.cart_data(request, cart)
            return HttpResponse(simplejson.dumps(data, cls=DjangoJSONEncoder))
        else:
            print "\033[91mApp or model does not exist.\033[0m"
            return HttpResponse(status=403)


class CartDeleteView(BaseCartView):
    def post(self, request, *args, **kwargs):
        app_label = request.POST.get('app_label')
        model_name = request.POST.get('model')
        object_id = request.POST.get('id')
        model = models.get_model(app_label, model_name)
        if model and model_name in self.availible_models:
            product = get_object_or_404(model, id=object_id)
            cart = Cart.objects.get_for_session(request.session)
            cart.delete_item(product)
            data = self.cart_data(request, cart)
            return HttpResponse(simplejson.dumps(data, cls=DjangoJSONEncoder))
        else:
            print "\033[91mApp or model does not exist.\033[0m"
            return HttpResponse(status=403)


class CartShowView(View):
    template_name = "cart/cart.html"

    def get(self, request, *args, **kwargs):
        ctx = {}
        ctx['cart'] = Cart.objects.get_for_session(request.session)
        ctx['cart_items'] = ctx['cart'].items.all()
        return render(request, self.template_name, ctx)
