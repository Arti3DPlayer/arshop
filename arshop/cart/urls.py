# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import CartGetView, CartAddView, CartDeleteView, CartShowView

urlpatterns = patterns('',
    url(r'^cart/get/$', CartGetView.as_view(), name='cart_get'),
    url(r'^cart/add/$', CartAddView.as_view(), name='cart_add'),
    url(r'^cart/delete/$', CartDeleteView.as_view(), name='cart_delete'),
    url(r'^cart/$', CartShowView.as_view(), name='cart_show'),
)
