from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.sitemaps import FlatPageSitemap
from arshop.sitemap import CatalogSitemap

admin.autodiscover()

urlpatterns = patterns('',
    url(r'', include('arshop.main.urls', 'main')),
    url(r'', include('arshop.catalog.urls', 'catalog')),
    url(r'', include('arshop.accounts.urls', 'accounts')),
    url(r'', include('arshop.cart.urls', 'cart')),
    url(r'', include('arshop.order.urls', 'order')),
    url(r'', include('arshop.search.urls', 'search')),

    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^rosetta/', include('rosetta.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += patterns('django.contrib.flatpages.views',
    (r'^(?P<url>.*/)$', 'flatpage'),
)

sitemaps = {
    'flatpages': FlatPageSitemap,
    'catalog':CatalogSitemap
}

urlpatterns += patterns('django.contrib.sitemaps.views',
    (r'^sitemap\.xml$', 'sitemap', {'sitemaps':sitemaps}),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
    )
