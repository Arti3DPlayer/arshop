# -*- coding: utf-8 -*-
from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from arshop.catalog.models import Product


class CatalogSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.5

    def items(self):
        return Product.objects.filter(is_hidden=False).order_by('pub_date')

    def lastmod(self, obj):
        return obj.pub_date

    def location(self, obj):
        return reverse('catalog:product_list', kwargs={'slug': obj.slug})
