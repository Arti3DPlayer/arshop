# -*- coding: utf-8 -*-
from modeltranslation.translator import translator, TranslationOptions

from .models import ExtendFlatPage, NavigationMenuItem, Chunk, State, Country,\
                    MainSlider

class ExtendFlatPageTranslationOptions(TranslationOptions):
    fields = ('title', 'keywords', 'description', 'content',)

class NavigationMenuItemTranslationOptions(TranslationOptions):
    fields = ('title',)

class ChunkTranslationOptions(TranslationOptions):
    fields = ('content',)

class StateTranslationOptions(TranslationOptions):
    fields = ('name',)

class CountryTranslationOptions(TranslationOptions):
    fields = ('name',)

class MainSliderTranslationOptions(TranslationOptions):
    fields = ('title', 'description')

translator.register(ExtendFlatPage, ExtendFlatPageTranslationOptions)
translator.register(NavigationMenuItem, NavigationMenuItemTranslationOptions)
translator.register(Chunk, ChunkTranslationOptions)
translator.register(State, StateTranslationOptions)
translator.register(Country, CountryTranslationOptions)
translator.register(MainSlider, MainSliderTranslationOptions)
