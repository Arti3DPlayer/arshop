# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage

class ExtendFlatPage(FlatPage):
    keywords = models.CharField(max_length=255, verbose_name=_(u'Keywords'),blank=True, null=True,)
    description = models.TextField(verbose_name=_(u'Description'),blank=True, null=True,)

    class Meta:
        verbose_name = _(u'Простая страница')
        verbose_name_plural = _(u'Простые страницы')

class NavigationMenu(models.Model):
    title = models.CharField(max_length=255, verbose_name=_(u'Название меню'))

    def get_childrens(self):
        return NavigationMenuItem.objects.filter(menu=self).order_by('priority')

    class Meta:
        verbose_name = _(u'Навигация')
        verbose_name_plural = _(u'Навигация')

    def __unicode__(self):
        return self.title

class NavigationMenuItem(models.Model):
    menu = models.ForeignKey('NavigationMenu', verbose_name=_(u'Меню'))
    title = models.CharField(max_length=255, verbose_name=_(u'Название'))
    slug = models.CharField(max_length=255, verbose_name=_(u'URL'), help_text=_(u"Example: '/about/contact/'. Make sure to have leading and trailing slashes."))
    is_blank = models.BooleanField(default=False, verbose_name=_(u'Открывать ссылку в новом окне'))
    priority = models.IntegerField(default=0, verbose_name=_(u'Позиция'))

    class Meta:
        verbose_name = _(u'Обьект навигации')
        verbose_name_plural = _(u'Обьекты навигации')


class Chunk(models.Model):
    key = models.CharField(verbose_name=_(u'Ключ'), help_text=_(u"Ключ должен быть уникальным."), blank=False, max_length=255, unique=True)
    content = models.TextField(verbose_name=_(u'Контент'), blank=True)

    class Meta:
        verbose_name = _(u'Чанк')
        verbose_name_plural = _(u'Чанки')

    def __unicode__(self):
        return u"%s" % (self.key,)

class Country(models.Model):
    name = models.CharField(max_length=30, verbose_name=_(u'Страна'))

    class Meta:
        verbose_name = _(u'Страна')
        verbose_name_plural = _(u'Страны')

    def __unicode__(self):
        return self.name

class State(models.Model):
    name = models.CharField(max_length=30, verbose_name=_(u'Город'))
    country = models.ForeignKey(Country)

    class Meta:
        verbose_name = _(u'Город')
        verbose_name_plural = _(u'Города')

    def __unicode__(self):
        return self.name

class Subscribe(models.Model):
    email = models.EmailField(max_length=60, verbose_name=_(u'E-mail'))
    is_active = models.BooleanField(default=True, verbose_name=_(u'Активная'))
    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=_(u'Дата создания'))

    class Meta:
        verbose_name = _(u'Подписка')
        verbose_name_plural = _(u'Подписки')

    def __unicode__(self):
        return self.email


class MainSlider(models.Model):
    title = models.CharField(max_length=255, verbose_name=_(u'Название'))
    description = models.TextField(verbose_name=_(u'Описание'))
    image = models.ImageField(upload_to='images/', verbose_name=_(u'Изображение'))
    priority = models.IntegerField(default=0, verbose_name=_(u'Позиция'))

    class Meta:
        verbose_name = _(u'Слайд')
        verbose_name_plural = _(u'Главный слайдер')

    def __unicode__(self):
        return self.title
