# -*- coding: utf-8 -*-
from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from django.utils.translation import ugettext_lazy as _
from django import forms

from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from .models import ExtendFlatPage, NavigationMenu, NavigationMenuItem, Chunk, \
                    State, Country, Subscribe, MainSlider

class NavigationMenuItemForm(forms.ModelForm):
    slug = forms.RegexField(label=_("URL"), max_length=100, regex=r'^[-\w/\.~]+$',
        help_text = _("Example: '/about/contact/'. Make sure to have leading"
                      " and trailing slashes."),
        error_message = _("This value must contain only letters, numbers,"
                          " dots, underscores, dashes, slashes or tildes."))
    class Meta:
        model = NavigationMenuItem


class NavigationMenuItemInline(admin.StackedInline):
    model = NavigationMenuItem
    form = NavigationMenuItemForm
    extra = 1

class NavigationMenuAdmin(admin.ModelAdmin):
    inlines = [NavigationMenuItemInline, ]

class ExtendFlatPageForm(FlatpageForm):
    class Meta:
        model = ExtendFlatPage

class ExtendFlatPageAdmin(FlatPageAdmin, TranslationAdmin):
    form = ExtendFlatPageForm
    fieldsets = [
        (None, {
            'fields':
                ['url', 'title', 'keywords', 'description',\
                     'content', 'sites', 'template_name',
                ]
        }),
    ]


class ChunkAdmin(TranslationAdmin):
    list_display = ('key',)
    search_fields = ('key', 'content')

class StateInline(admin.StackedInline):
    model = State
    extra = 1

class CountryAdmin(TranslationAdmin):
    model = Country
    inlines = [StateInline, ]


class SubscribeAdmin(admin.ModelAdmin):
    model = Subscribe


class MainSliderAdmin(TranslationAdmin):
    model = MainSlider
    list_display = ('title', 'priority')

admin.site.unregister(FlatPage)
admin.site.register(ExtendFlatPage, ExtendFlatPageAdmin)
admin.site.register(NavigationMenu, NavigationMenuAdmin)
admin.site.register(Chunk, ChunkAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(Subscribe, SubscribeAdmin)
admin.site.register(MainSlider, MainSliderAdmin)
