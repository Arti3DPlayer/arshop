# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import LanguageView, MainPageView

urlpatterns = patterns('',
    url(r'^$', MainPageView.as_view(), name='main_page'),
    url(r'^lang/(?P<code>[a-z]{2})/$', LanguageView.as_view(), name='lang'),
)
