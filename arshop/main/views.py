# -*- coding: utf-8 -*-
from django.shortcuts import redirect, get_object_or_404, render
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic import ListView, DetailView, FormView, TemplateView
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.conf import settings

from arshop.catalog.models import Product
from .models import MainSlider

class MainPageView(View):
    template_name="main/index.html"

    def get(self, request, *args, **kwargs):
        ctx = dict()
        ctx['popular_products'] = Product.objects.filter(is_popular=True, is_hidden=False)
        ctx['slider_items'] = MainSlider.objects.order_by('priority')
        return render(request, self.template_name, ctx)


class LanguageView(View):
    """
    Change language view.
    """
    def get(self, request, *args, **kwargs):
        code = kwargs['code']
        next = request.META.get('HTTP_REFERER', '/')
        response = HttpResponseRedirect(next)
        if code and translation.check_for_language(code):
            if hasattr(request, 'session'):
                request.session['django_language'] = code
            else:
                response.set_cookie(settings.LANGUAGE_COOKIE_NAME, code)
            translation.activate(code)
        return response
