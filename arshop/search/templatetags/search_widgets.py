# -*- coding: utf-8 -*-
from django import template
from django.utils.translation import ugettext_lazy as _
from arshop.search.forms import SearchForm

register = template.Library()

@register.inclusion_tag('search/inclusion/search_form.html')
def search_form_tag():
    return {'form':SearchForm()}
