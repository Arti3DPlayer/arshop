# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _

class SearchForm(forms.Form):
    search = forms.CharField(max_length=255, label=_(u'Поиск'))
