# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import Order


class OrderForm(forms.ModelForm):
    payment = forms.ChoiceField(widget=forms.RadioSelect(), choices=Order.PAYMENT_CHOICES)

    def __init__(self, *args, **kwargs):
        super(OrderForm, self).__init__(*args, **kwargs)
        self.fields['delivery'].empty_label = None

    class Meta:
        model = Order
        widgets = {
            'delivery': forms.RadioSelect(),
        }
        exclude = ['status', ]
