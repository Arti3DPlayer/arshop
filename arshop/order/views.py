# -*- coding: utf-8 -*-
from django.shortcuts import redirect, get_object_or_404, render
from django.core.urlresolvers import reverse_lazy
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic import ListView, DetailView, FormView, TemplateView
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.views import login
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail
from django.conf import settings

from .forms import OrderForm
import paypalrestsdk
from paypalrestsdk.openid_connect import Tokeninfo, Userinfo
import urllib


class CheckOutView(View):
    template_name = "order/checkout.html"
    auth_form_class = AuthenticationForm
    order_form_class = OrderForm

    def get(self, request, *args, **kwargs):
        ctx = dict()
        ctx['auth_form'] = self.auth_form_class()
        if request.user.is_authenticated():
            ctx['order_form'] = self.order_form_class(instance=request.user)
        else:
            ctx['order_form'] = self.order_form_class()
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = dict()
        if request.POST.get('auth_form'):
            auth_form = self.auth_form_class(data=request.POST)
            if auth_form.is_valid():
                login(request, auth_form.get_user())
                return redirect(reverse_lazy('order:checkout'))
            ctx['auth_form'] = auth_form
        elif request.POST.get('order_form'):
            order_form = self.order_form_class(request.POST)
            if order_form.is_valid():
                CLIENT_ID = "AXO8sBAyy9O7EmkRldu9hVV7XEzCOFfMnmR1FCyGfjrSQnTDRDw27NK-zHSs"
                CLIENT_SECRET = "EEXYzhD9QzC8xHiMEKmpqEMNMDrnJs92s0h4nOrDNGuIsOWzqGwY7I_we_uM"
                paypalrestsdk.configure({
                    "mode": "sandbox",
                    "client_id": CLIENT_ID,
                    "client_secret": CLIENT_SECRET,
                    "openid_redirect_uri": "https://devtools-paypal.com/" })
                # Generate login url
                login_url = Tokeninfo.authorize_url({ "scope": "openid profile"})
                return HttpResponseRedirect(login_url)
                # Create tokeninfo with Authorize code
                tokeninfo = Tokeninfo.create(CLIENT_SECRET)
                print tokeninfo
                # Refresh tokeninfo
                tokeninfo = tokeninfo.refresh()

                # Create tokeninfo with refresh_token
                tokeninfo = Tokeninfo.create_with_refresh_token("Replace with refresh_token")

                # Get userinfo
                userinfo  = tokeninfo.userinfo()

                # Get userinfo with access_token
                userinfo  = Userinfo.get("Replace with access_token")

                # Generate logout url
                logout_url = tokeninfo.logout_url()
            ctx['order_form'] = order_form
        else:
            ctx['auth_form'] = self.auth_form_class()
            ctx['order_form'] = self.order_form_class()
        return render(request, self.template_name, ctx)
