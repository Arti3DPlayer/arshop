# -*- coding: utf-8 -*-
from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from django.utils.translation import ugettext_lazy as _
from django import forms
from .models import Delivery, Order

class DeliveryAdmin(admin.ModelAdmin):
    model = Delivery

class OrderAdmin(admin.ModelAdmin):
    model = Order

admin.site.register(Delivery, DeliveryAdmin)
admin.site.register(Order, OrderAdmin)
