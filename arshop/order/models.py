# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from arshop.main.models import Country, State

class Delivery(models.Model):
    title = models.CharField(max_length=30, verbose_name=_(u'Название компании'))
    url = models.URLField(max_length=255, null=True, blank=True, verbose_name=_(u'Веб-сайт компании'))
    price = models.IntegerField(default=0, verbose_name=_(u'Цена'))

    class Meta:
        verbose_name = u'Компания для доставки'
        verbose_name_plural = u'Компании для доставки'

    def __unicode__(self):
        return self.title


class Order(models.Model):
    PAYMENT_CHOICES = (('cc', 'Credit Card'),
                    ('pp', 'Paypall'),
                    ('cis', 'Collect in store'))

    STATUS_CHOICES = (('1', 'Google checkout'),
                    ('2', 'Paypall'),
                    ('3', 'Collect in store'))
    first_name = models.CharField(max_length=255, verbose_name=_(u'Имя'))
    last_name = models.CharField(max_length=255, verbose_name=_(u'Фамилия'))
    email = models.CharField(max_length=255, verbose_name=_(u'E-mail'))
    phone = models.CharField(max_length=255, verbose_name=_(u'Телефон'))
    address1 = models.CharField(max_length=255, verbose_name=_(u'Адресс'))
    address2 = models.CharField(max_length=255, verbose_name=_(u'Адресс1'))
    city = models.ForeignKey(State, verbose_name=_(u'Город'))
    zip_address = models.CharField(max_length=255, verbose_name=_(u'Почтовый индекс'))
    country = models.ForeignKey(Country, verbose_name=_(u'Страна'))
    delivery = models.ForeignKey(Delivery, verbose_name=_(u'Компания для доставки'))
    payment = models.CharField(max_length=20, verbose_name=u'Оплата', choices=PAYMENT_CHOICES)
    status = models.CharField(max_length=20, verbose_name=u'Статус', choices=STATUS_CHOICES)


    class Meta:
        verbose_name = _(u'Заказ')
        verbose_name_plural = _(u'Заказы')

    def __unicode__(self):
        return self.id
