# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import CheckOutView

urlpatterns = patterns('',
    url(r'^order/checkout/$', CheckOutView.as_view(), name='checkout'),
)
