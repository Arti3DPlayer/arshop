# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Renaming column for 'Order.city' to match new field type.
        db.rename_column(u'order_order', 'city', 'city_id')
        # Changing field 'Order.city'
        db.alter_column(u'order_order', 'city_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.State']))
        # Adding index on 'Order', fields ['city']
        db.create_index(u'order_order', ['city_id'])


        # Renaming column for 'Order.country' to match new field type.
        db.rename_column(u'order_order', 'country', 'country_id')
        # Changing field 'Order.country'
        db.alter_column(u'order_order', 'country_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Country']))
        # Adding index on 'Order', fields ['country']
        db.create_index(u'order_order', ['country_id'])


    def backwards(self, orm):
        # Removing index on 'Order', fields ['country']
        db.delete_index(u'order_order', ['country_id'])

        # Removing index on 'Order', fields ['city']
        db.delete_index(u'order_order', ['city_id'])


        # Renaming column for 'Order.city' to match new field type.
        db.rename_column(u'order_order', 'city_id', 'city')
        # Changing field 'Order.city'
        db.alter_column(u'order_order', 'city', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Renaming column for 'Order.country' to match new field type.
        db.rename_column(u'order_order', 'country_id', 'country')
        # Changing field 'Order.country'
        db.alter_column(u'order_order', 'country', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        u'main.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_zh_cn': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'main.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_zh_cn': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'order.delivery': {
            'Meta': {'object_name': 'Delivery'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'order.order': {
            'Meta': {'object_name': 'Order'},
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.State']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Country']"}),
            'delivery': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['order.Delivery']"}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'payment': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'zip_address': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['order']