# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from .models import User


class AdminUserAddForm(UserCreationForm):

    class Meta:
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


class AdminUserChangeForm(UserChangeForm):

    class Meta:
        model = User


class RegistrationForm(UserCreationForm):
    privace_policy = forms.BooleanField(label=_(u'I have read and agree to the Privacy Policy'))
    subscribe = forms.BooleanField(required=False, label=_(u'Subscribe to our newsletter '))

    class Meta:
        model = User
        exclude = ('date_joined', 'last_login', 'is_superuser',\
                    'user_permissions', 'is_staff', 'is_active',\
                     'groups', 'password' )
