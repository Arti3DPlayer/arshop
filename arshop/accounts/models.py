# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from arshop.main.models import Country, State


AbstractUser._meta.get_field('email')._unique = True
AbstractUser._meta.get_field('email').blank = False


class User(AbstractUser):
    phone = models.CharField(max_length=255, verbose_name=_(u'Телефон'), blank=True)
    address1 = models.CharField(max_length=255, verbose_name=_(u'Адресс'), blank=True)
    address2 = models.CharField(max_length=255, verbose_name=_(u'Адресс1'), blank=True)
    city = models.ForeignKey(State, verbose_name=_(u'Город'), blank=True, null=True)
    zip_address = models.CharField(max_length=255, verbose_name=_(u'Почтовый индекс'), blank=True)
    country = models.ForeignKey(Country, verbose_name=_(u'Страна'), blank=True, null=True)
