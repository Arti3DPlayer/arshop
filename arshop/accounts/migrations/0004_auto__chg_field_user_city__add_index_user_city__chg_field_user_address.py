# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Renaming column for 'User.city' to match new field type.
        db.rename_column(u'accounts_user', 'city', 'city_id')
        # Changing field 'User.city'
        db.alter_column(u'accounts_user', 'city_id', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['main.State']))
        # Adding index on 'User', fields ['city']
        db.create_index(u'accounts_user', ['city_id'])


        # Changing field 'User.address1'
        db.alter_column(u'accounts_user', 'address1', self.gf('django.db.models.fields.CharField')(default='', max_length=255))

        # Changing field 'User.address2'
        db.alter_column(u'accounts_user', 'address2', self.gf('django.db.models.fields.CharField')(default='', max_length=255))

        # Changing field 'User.zip_address'
        db.alter_column(u'accounts_user', 'zip_address', self.gf('django.db.models.fields.CharField')(default='', max_length=255))

        # Changing field 'User.phone'
        db.alter_column(u'accounts_user', 'phone', self.gf('django.db.models.fields.CharField')(default='', max_length=255))

        # Renaming column for 'User.country' to match new field type.
        db.rename_column(u'accounts_user', 'country', 'country_id')
        # Changing field 'User.country'
        db.alter_column(u'accounts_user', 'country_id', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['main.Country']))
        # Adding index on 'User', fields ['country']
        db.create_index(u'accounts_user', ['country_id'])


    def backwards(self, orm):
        # Removing index on 'User', fields ['country']
        db.delete_index(u'accounts_user', ['country_id'])

        # Removing index on 'User', fields ['city']
        db.delete_index(u'accounts_user', ['city_id'])


        # Renaming column for 'User.city' to match new field type.
        db.rename_column(u'accounts_user', 'city_id', 'city')
        # Changing field 'User.city'
        db.alter_column(u'accounts_user', 'city', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'User.address1'
        db.alter_column(u'accounts_user', 'address1', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'User.address2'
        db.alter_column(u'accounts_user', 'address2', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'User.zip_address'
        db.alter_column(u'accounts_user', 'zip_address', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'User.phone'
        db.alter_column(u'accounts_user', 'phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Renaming column for 'User.country' to match new field type.
        db.rename_column(u'accounts_user', 'country_id', 'country')
        # Changing field 'User.country'
        db.alter_column(u'accounts_user', 'country', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    models = {
        u'accounts.user': {
            'Meta': {'object_name': 'User'},
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.State']", 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Country']", 'blank': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'zip_address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_zh_cn': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'main.state': {
            'Meta': {'object_name': 'State'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'name_zh_cn': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['accounts']