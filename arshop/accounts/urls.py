# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import ProfileView, SignIn, SignUp, LogOut

urlpatterns = patterns('',
    url(r'^profile/$', ProfileView.as_view(), name='profile'),
    url(r'^accounts/signin/$', SignIn.as_view(), name='signin'),
    url(r'^accounts/signup/$', SignUp.as_view(), name='signup'),
    url(r'^accounts/logout/$', LogOut.as_view(), name= 'logout'),
)
