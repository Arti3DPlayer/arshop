# -*- coding: utf-8 -*-
from django.shortcuts import redirect, render
from django.core.urlresolvers import reverse_lazy
from django.contrib import auth
from django.views.generic.base import View
from django.views.generic.edit import FormView

from django.contrib.auth.forms import AuthenticationForm

from .models import User
from .utils import LoginRequiredMixin
from .forms import RegistrationForm


class ProfileView(LoginRequiredMixin, View):
    template_name = 'accounts/profile.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})


class SignIn(FormView):
    form_class = AuthenticationForm
    template_name = 'accounts/signin.html'

    def get_success_url(self):
        if self.request.GET.get('next'):
            self.success_url = self.request.GET.get('next')
        else:
            self.success_url = '/'
        return self.success_url

    def form_valid(self, form):
        cd = form.cleaned_data
        user = auth.authenticate(username=cd['username'], password=cd['password'])
        auth.login(self.request, user)
        return super(SignIn, self).form_valid(form)


class SignUp(FormView):
    form_class = RegistrationForm
    template_name = 'accounts/signup.html'
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        cd = form.cleaned_data
        User.objects.create_user(
            username=cd['username'],
            email=cd['email'],
            password=cd['password1'],
            first_name=cd['first_name'],
            last_name=cd['last_name'],
            phone=cd['phone'],
            address1=cd['address1'],
            address2=cd['address2'],
            city=cd['city'],
            country=cd['country'],
            zip_address=cd['zip_address'])
        user = auth.authenticate(username=cd['username'], password=cd['password1'])
        auth.login(self.request, user)
        return super(SignUp, self).form_valid(form)


class LogOut(View):
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            auth.logout(request)
        return redirect(request.META.get('HTTP_REFERER'))
