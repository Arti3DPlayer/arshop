# -*- coding: utf-8 -*-
from django import template
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _
from arshop.catalog.models import Category

register = template.Library()

@register.assignment_tag
def category_list_tag():
    return Category.objects.all().order_by("priority")\
            .annotate(product_count=Count('product'))
