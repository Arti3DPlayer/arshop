# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from .views import ProductList, ProductDetail

urlpatterns = patterns('',
    url(r'^catalog/product/list/(?P<slug>[\w\-]+)/$', ProductList.as_view(), name='product_list'),
    url(r'^catalog/product/(?P<slug>[\w\-]+)/$', ProductDetail.as_view(), name='product_detail'),
)
