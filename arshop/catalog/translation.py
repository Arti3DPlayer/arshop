# -*- coding: utf-8 -*-
from modeltranslation.translator import translator, TranslationOptions

from .models import Category, Product, ProductFeatures

class CategoryTranslationOptions(TranslationOptions):
    fields = ('title',)

class ProductFeaturesTranslationOptions(TranslationOptions):
    fields = ('title', 'description')

class ProductTranslationOptions(TranslationOptions):
    fields = ('title', 'short_description', 'description')

translator.register(Category, CategoryTranslationOptions)
translator.register(ProductFeatures, ProductFeaturesTranslationOptions)
translator.register(Product, ProductTranslationOptions)
