# -*- coding: utf-8 -*-
from django.contrib import admin
from mptt.admin import MPTTModelAdmin
from django.utils.translation import ugettext_lazy as _
from django import forms
from modeltranslation.admin import TranslationAdmin
from .models import Category, Product, ProductFeatures, ProductImage

class CategoryForm(forms.ModelForm):
    slug = forms.RegexField(label=_("URL"), max_length=100, regex=r'^[-\w/\.~]+$',
        help_text = _("Example: '/about/contact/'. Make sure to have leading"
                      " and trailing slashes."),
        error_message = _("This value must contain only letters, numbers,"
                          " dots, underscores, dashes, slashes or tildes."))
    class Meta:
        model = Category

class CategoryMPTTModelAdmin(MPTTModelAdmin, TranslationAdmin):
    form = CategoryForm
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'priority')
    list_editable = ('priority',)

class ProductImageAdmin(admin.TabularInline):
    model=ProductImage
    extra=0


class ProductFeaturesAdmin(admin.StackedInline):
    model=ProductFeatures
    extra=0

class ProductForm(forms.ModelForm):
    slug = forms.RegexField(label=_("URL"), max_length=100, regex=r'^[-\w/\.~]+$',
        help_text = _("Example: '/about/contact/'. Make sure to have leading"
                      " and trailing slashes."),
        error_message = _("This value must contain only letters, numbers,"
                          " dots, underscores, dashes, slashes or tildes."))
    class Meta:
        model = Category
        exclude = ['rate', 'rate_count', ]

class ProductAdmin(TranslationAdmin):
    inlines = [ProductFeaturesAdmin, ProductImageAdmin, ]
    form = ProductForm
    model=Product
    list_display = ('title', 'code', 'category', 'admin_image_tag', 'count', 'is_hidden', )
    list_editable = ('count', 'is_hidden', )
    prepopulated_fields = {"slug": ("title",)}
    filter_horizontal = ('same_products', )
    search_fields = ('title', 'description', )
    list_filter = ('is_hidden', 'category')

admin.site.register(Category, CategoryMPTTModelAdmin)
admin.site.register(Product, ProductAdmin)
