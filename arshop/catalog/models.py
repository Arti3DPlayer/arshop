# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey

class Category(MPTTModel):
    title = models.CharField(max_length=255, verbose_name=_(u'Название'))
    slug = models.CharField(max_length=255, verbose_name=_(u'URL'), unique=True)
    is_blank = models.BooleanField(default=False, verbose_name=_(u'Открывать ссылку в новом окне'))
    priority = models.IntegerField(default=0, verbose_name=_(u'Позиция'))
    parent = TreeForeignKey('self', verbose_name=_(u'Родитель'), null=True, blank=True, related_name='children')

    class MPTTMeta:
        level_attr = 'mptt_level'

    class Meta:
        verbose_name = _(u'Категория')
        verbose_name_plural = _(u'Категории')

    def __unicode__(self):
        return self.title

class Product(models.Model):
    title = models.CharField(max_length=255, verbose_name=_(u'Название'))
    code = models.CharField(max_length=255, verbose_name=_(u'Код продукта'), unique=True)
    slug = models.CharField(max_length=255, verbose_name=_(u'URL'), unique=True)
    category = models.ForeignKey('Category', verbose_name=_(u'Категория'))
    short_description = models.TextField(verbose_name=_(u'Краткое описание'))
    description = models.TextField(verbose_name=_(u'Описание'))
    count = models.IntegerField(default=0, verbose_name=_(u'Количество на складе'))
    price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2, verbose_name=_(u'Стоимость'), help_text=_(u'$'))
    same_products = models.ManyToManyField('self', verbose_name=_(u'Похожие товары'), blank=True, null=True)
    rate = models.IntegerField(default=0, verbose_name=_(u'Рейтинг'))
    rate_count = models.IntegerField(default=0, verbose_name=_(u'Рейтинг'))
    is_hidden = models.BooleanField(default=False, verbose_name=_(u'Скрыть'))
    is_popular = models.BooleanField(default=False, verbose_name=_(u'Популярный продукт'))
    pub_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=_(u'Дата создания'))

    def admin_image_tag(self):
        return u'<img src="%s" width="128" height="128" />' % self.get_main_image()

    admin_image_tag.short_description = _(u'Изображение')
    admin_image_tag.allow_tags = True

    def get_main_image(self):
        image = ProductImage.objects.filter(product=self, is_main=True)
        if not image:
            image = ProductImage.objects.filter(product=self)
            if not image:
                return '%s/css/images/no-image.png' % settings.STATIC_URL
        return image[0].image.url

    def get_images(self):
        return ProductImage.objects.filter(product=self)

    def get_features(self):
        return ProductFeatures.objects.filter(product=self)

    class Meta:
        verbose_name = _(u'Товар')
        verbose_name_plural = _(u'Товары')

    def __unicode__(self):
        return self.title

class ProductFeatures(models.Model):
    title = models.CharField(max_length=255, verbose_name=_(u'Название'))
    description = models.TextField(verbose_name=_(u'Описание'))
    product = models.ForeignKey('Product', verbose_name=_(u'Характеристики'))

    class Meta:
        verbose_name = _(u'Характеристика')
        verbose_name_plural = _(u'Характеристики')

    def __unicode__(self):
        return self.title

class ProductImage(models.Model):
    image = models.ImageField(upload_to='images/', verbose_name=_(u'Изображение'))
    is_main = models.BooleanField(default=False, verbose_name=_(u'Выбрать главным'))
    product = models.ForeignKey('Product', verbose_name=_(u'Характеристики'))

    class Meta:
        verbose_name = _(u'Изображение')
        verbose_name_plural = _(u'Изображения')

    def __unicode__(self):
        return self.image



