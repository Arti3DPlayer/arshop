# -*- coding: utf-8 -*-
from django.shortcuts import redirect, get_object_or_404, render
from django.contrib import auth, messages
from django.utils.decorators import method_decorator
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.views.generic.base import View
from django.views.generic import ListView, DetailView, FormView, TemplateView
from django.db.models import Count, Sum, Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.conf import settings

from .models import Category, Product


class ProductList(ListView):
    model = Product
    context_object_name = 'products'
    template_name = "catalog/product_list.html"
    paginate_by = 10

    def get_queryset(self):
        category = get_object_or_404(Category, slug=self.kwargs['slug'])
        descendants = category.get_descendants(include_self=True)
        return Product.objects.filter(category__in=descendants, is_hidden=False)


class ProductDetail(DetailView):
    model = Product
    template_name = "catalog/product_detail.html"

