(function($) {
    // Plugin
    jQuery.fn.cart = function(option) {
        return this.each(function() {
            var $this = $(this);

            var data = $this.data('cart');
            if (!data) $this.data('cart', (data = new Cart(this, option)));
        });
    };

    // Initialization
    var Cart = function(element, options)
    {
        // Element
        this.$el = $(element);

        // Options
        this.opts = $.extend({
            get_cart_url: '/cart/get/',
            add_cart_url: '/cart/add/',
            delete_cart_url: '/cart/delete/',
            loader_template: function () {
                $(element).html('<div>Loading...</div>');
            },

            cart_template: function(data) {
                $(element).html('Корзина ('+data['quantity']+')')
            },
        }, options, this.$el.data());

        // Init
        this.init();
    };

    // Functionality
    Cart.prototype = {

        getCookie: function(c_name)
        {
            if (document.cookie.length > 0)
            {
                c_start = document.cookie.indexOf(c_name + "=");
                if (c_start != -1)
                {
                    c_start = c_start + c_name.length + 1;
                    c_end = document.cookie.indexOf(";", c_start);
                    if (c_end == -1) c_end = document.cookie.length;
                    return unescape(document.cookie.substring(c_start,c_end));
                }
            }
            return "";
        },

        // Initialization
        init: function()
        {
            console.log("Cart", this.$el, "initialization...");
            console.log("Options:");
            console.table([this.opts],Object.keys(this.opts))
            console.log("Finish...");
            this.cart_update();
        },

        cart_update: function () {
            console.log("Cart updated...");
            var obj = this;
            $.ajax({
                headers: { "X-CSRFToken": obj.getCookie("csrftoken") },
                url: this.opts.get_cart_url,
                type : "POST",
                dataType: "json",
                beforeSend: function() {
                    obj.opts.loader_template();
                },
                success: function (data) {
                    console.log(data)
                    obj.opts.cart_template(data);
                }
            });
            console.log("Finish...");
        },

        add_to_cart: function(data) {
            console.log("Add object to cart...");
            var obj = this;
            var data = data;
            $.ajax({
                headers: { "X-CSRFToken": this.getCookie("csrftoken") },
                url: this.opts.add_cart_url,
                type : "POST",
                data : data,
                dataType: "json",
                beforeSend: function() {
                    obj.opts.loader_template();
                },
                success: function (data) {
                    console.log(data)
                    obj.opts.cart_template(data);
                }
            });
            console.log("Finish...");
        },

        delete_from_cart: function(data) {
            console.log("Remove object to cart...");
            var obj = this;
            var data = data;
            $.ajax({
                headers: { "X-CSRFToken": this.getCookie("csrftoken") },
                url: this.opts.delete_cart_url,
                type : "POST",
                data : data,
                dataType: "json",
                beforeSend: function() {
                    obj.opts.loader_template();
                },
                success: function (data) {
                    console.log(data)
                    obj.opts.cart_template(data);
                }
            });
            console.log("Finish...");
        },
    };

    // API
    $.fn.getCookie = function(c_name)
    {
        return this.data('cart').getCookie(c_name);
    };

    $.fn.cart_update = function()
    {
        return this.data('cart').cart_update();
    };

    $.fn.add_to_cart = function(data)
    {
        return this.data('cart').add_to_cart(data);
    };

    $.fn.delete_from_cart = function(data)
    {
        return this.data('cart').delete_from_cart(data);
    };


})(jQuery);
